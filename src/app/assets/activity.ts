export const schema = {
  "title": "Invoke REST Service",
  "version": "1.0.0",
  "type": "flogo:activity",
  "name": "tibco-wi-restinvoke",
  "author": "TIBCO Software Inc.",
  "display": {
    "visible": true,
    "description": "Simple REST Activity",
    "category": "General",
    "smallIcon": "icons/ic-tibco-wi-restinvoke.png",
    "largeIcon": "icons/ic-tibco-wi-restinvoke@2x.png"
  },
  "ref": "git.tibco.com/git/product/ipaas/wi-contrib.git/activity/general/rest",
  "inputs": [{
    "name": "Method",
    "type": "string",
    "required": true,
    "display": {
      "description": "The REST method used for the requests",
      "name": "Method",
      "type": "dropdwon",
      "selection": "single"
    },
    "allowed": [
      "GET",
      "POST",
      "PUT",
      "DELETE"
    ],
    "value": "GET"
  },
  {
    "name": "Uri",
    "type": "string",
    "required": true,
    "value": "Hola",
    "display": {
      "description": "The URL of the REST service",
      "name": "URL"
    }
  },
  {
    "name": "Use certificate for verification",
    "type": "boolean",
    "required": true,
    "value": false,
    "display": {
      "description": "Use certificate for secure connection to the server",
      "name": "Use certificate for verification",
      "visible": false
    }
  },
  {
    "name": "Server Certificate",
    "type": "string",
    "required": true,
    "display": {
      "description": "Self-signed PEM certificate for secure connection",
      "name": "Server Certificate",
      "visible": false,
      "type": "fileselector",
      "fileExtensions": [
        ".pem",
        ".cert",
        ".cer",
        ".crt"
      ]
    }
  },
  {
    "name": "queryParams",
    "type": "complex_object",
    "display": {
      "name": "Query Params",
      "description": "The query parameters for request",
      "type": "params",
      "schema": "{\"type\":\"array\",\"items\":{\"type\":\"object\",\"properties\":{\"parameterName\":{\"type\":\"string\"},\"type\":{\"type\":{\"enum\":[\"string\",\"number\",\"boolean\"]}},\"required\":{\"type\":{\"enum\":[\"true\",\"false\"]}}}}}",
      "mappable": true
    }
  },
  {
    "name": "pathParams",
    "type": "complex_object",
    "display": {
      "name": "Path Params",
      "description": "The path parameters for request",
      "type": "params",
      "readonly": true,
      "schema": "{\"type\": \"array\",\"items\": {\"type\": \"object\",\"properties\": {\"parameterName\": {\"type\": \"string\"},\"type\": {\"type\": {\"enum\": [\"string\"]}}}}}",
      "mappable": true
    }
  },
  {
    "name": "headers",
    "type": "complex_object",
    "display": {
      "name": "Request Headers",
      "description": "The headers you want to send",
      "type": "params",
      "schema": "{\"type\":\"array\",\"items\":{\"type\":\"object\",\"properties\":{\"parameterName\":{\"type\":\"string\"},\"type\":{\"type\":{\"enum\":[\"string\",\"number\",\"boolean\"]}},\"repeating\":{\"type\":{\"enum\":[\"true\",\"false\"]}},\"required\":{\"type\":{\"enum\":[\"true\",\"false\"]}}}}}",
      "mappable": true
    },
    "value": {
      "metadata": "",
      "value": "[{\"parameterName\":\"Accept\",\"type\":\"string\",\"repeating\":\"false\",\"required\":\"false\",\"visible\":false},{\"parameterName\":\"Accept-Charset\",\"type\":\"string\",\"repeating\":\"false\",\"required\":\"false\",\"visible\":false},{\"parameterName\":\"Accept-Encoding\",\"type\":\"string\",\"repeating\":\"false\",\"required\":\"false\",\"visible\":false},{\"parameterName\":\"Content-Type\",\"type\":\"string\",\"repeating\":\"false\",\"required\":\"false\",\"visible\":false},{\"parameterName\":\"Content-Length\",\"type\":\"string\",\"repeating\":\"false\",\"required\":\"false\",\"visible\":false},{\"parameterName\":\"Connection\",\"type\":\"string\",\"repeating\":\"false\",\"required\":\"false\",\"visible\":false},{\"parameterName\":\"Cookie\",\"type\":\"string\",\"repeating\":\"false\",\"required\":\"false\",\"visible\":false},{\"parameterName\":\"Pragma\",\"type\":\"string\",\"repeating\":\"false\",\"required\":\"false\",\"visible\":false}]"
    }
  },
  {
    "name": "body",
    "type": "complex_object",
    "display": {
      "name": "Request Schema",
      "description": "An example JSON data that you want to send to the REST service",
      "type": "texteditor",
      "syntax": "json",
      "mappable": true,
      "visible": false
    }
  }
  ],
  "outputs": [{
    "name": "statusCode",
    "type": "integer"
  },
  {
    "name": "responseBody",
    "type": "complex_object",
    "display": {
      "name": "Response Schema",
      "description": "An example JSON data that you expect back from the REST service",
      "type": "texteditor",
      "syntax": "json"
    }
  },
  {
    "name": "headers",
    "type": "complex_object",
    "display": {
      "name": "Response Headers",
      "description": "The headers you expect to receive",
      "type": "params",
      "schema": "{\"type\":\"array\",\"items\":{\"type\":\"object\",\"properties\":{\"parameterName\":{\"type\":\"string\"},\"type\":{\"type\":{\"enum\":[\"string\",\"number\",\"boolean\"]}},\"repeating\":{\"type\":{\"enum\":[\"true\",\"false\"]}},\"required\":{\"type\":{\"enum\":[\"true\",\"false\"]}}}}}"
    },
    "value": {
      "metadata": "",
      "value": "[{\"parameterName\":\"Accept\",\"type\":\"string\",\"repeating\":\"false\",\"required\":\"false\",\"visible\":false},{\"parameterName\":\"Accept-Charset\",\"type\":\"string\",\"repeating\":\"false\",\"required\":\"false\",\"visible\":false},{\"parameterName\":\"Accept-Encoding\",\"type\":\"string\",\"repeating\":\"false\",\"required\":\"false\",\"visible\":false},{\"parameterName\":\"Content-Type\",\"type\":\"string\",\"repeating\":\"false\",\"required\":\"false\",\"visible\":false},{\"parameterName\":\"Content-Length\",\"type\":\"string\",\"repeating\":\"false\",\"required\":\"false\",\"visible\":false},{\"parameterName\":\"Connection\",\"type\":\"string\",\"repeating\":\"false\",\"required\":\"false\",\"visible\":false},{\"parameterName\":\"Cookie\",\"type\":\"string\",\"repeating\":\"false\",\"required\":\"false\",\"visible\":false},{\"parameterName\":\"Pragma\",\"type\":\"string\",\"repeating\":\"false\",\"required\":\"false\",\"visible\":false}]"
    }
  },
  {
    "name": "error",
    "type": "string"
  }
  ]
}
