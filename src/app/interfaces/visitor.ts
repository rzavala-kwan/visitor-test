export interface ISchema {
  accept(visitor: IVisitor);
}

export interface IVisitor {
  visitTrigger(trigger: Trigger);
  visitActivity(activity: Activity);
  visitConnector(connector: Connector);
}

export class Trigger implements ISchema {
  accept(visitor: IVisitor){
    visitor.visitTrigger(this);
  }
}

export class Activity implements ISchema {
  accept(visitor: IVisitor){
    visitor.visitActivity(this);
  }
}

export class Connector implements ISchema {
  accept(visitor: IVisitor){
    visitor.visitConnector(this);
  }
}

export class IsValidVisitor implements IVisitor {
  visitActivity(activity: Activity){
    console.log("ACTIVITY VALID!");
  }
  visitTrigger(trigger: Trigger){
    // TRIGGER IS VALID!
  }
  visitConnector(connector: Connector){
    // CONNECTOR IS VALID!
  }
}

export class IsNotValidVisitor implements IVisitor {
   visitActivity(activity: Activity){
    // ACTIVITY IS NOT VALID!
  }
  visitTrigger(trigger: Trigger){
    // TRIGGER IS NOT VALID!
  }
  visitConnector(connector: Connector){
    // CONNECTOR IS NOT VALID!
  }
}

export class Validator {
  // Load json/schema
  schemaType:string;

  constructor(schemaType:string) {
    this.schemaType = schemaType;
  }

  validVisitor = new IsValidVisitor();
  invalidVisitor = new IsNotValidVisitor();
  
  validateActivity() {
    // let schemaTypeDummy = "flogo:activity";
    let activitySchema = new Activity;

    /* --Dummy-- 
      if (schemaTypeDummy === "flogo:activity") {
        activitySchema.accept(validVisitor);
      } else {
        activitySchema.accept(invalidVisitor);
      }
    */

    if (this.schemaType === "flogo:activity") {
      activitySchema.accept(this.validVisitor);
    } else {
      activitySchema.accept(this.invalidVisitor);
    }

  }

  validateTrigger() {
    let triggerSchema = new Trigger;

    if (this.schemaType === "flogo:trigger") {
      triggerSchema.accept(this.validVisitor);
    } else {
      triggerSchema.accept(this.invalidVisitor);
    }
  }

  validateConnector() {
    let connectorSchema = new Connector;

    if (this.schemaType === "flogo:connector") {
      connectorSchema.accept(this.validVisitor);
    } else {
      connectorSchema.accept(this.invalidVisitor);
    }
  }
  
}
/* var myString = "flogo:activity";
let validator = new Validator(myString)
validator.validateActivity(); */